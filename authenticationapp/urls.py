from django.urls import path
from authenticationapp import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('signup/', views.sign_up,name='signup'),
    path('login/',views.log_in, name='login'),
    path('profile/', views.profile, name='profile'),
    path('logout/', views.log_out, name='logout'),
    path('changepass/', views.change_password_with_old_password,name='changepassword'),
    path('',views.home, name='home'),
    path('delete_post/<int:post_id>/', views.delete_post, name='delete'),
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name='authenticationapp/password_reset_form.html'), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='authenticationapp/password_reset_done.html'), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='authenticationapp/password_reset_confirm.html'), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name='authenticationapp/password_reset_complete.html'), name='password_reset_complete'),
]