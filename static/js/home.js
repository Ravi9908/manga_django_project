document.addEventListener("DOMContentLoaded", () => {
  const menuBtn = document.querySelector(".menu-btn");
  const navbar = document.querySelector(".navbar");
  const closeBtn = document.querySelector(".close-btn");

  menuBtn.addEventListener("click", () => {
    menuBtn.style.display = "none";
    navbar.style.display = "block";
  });
  closeBtn.addEventListener("click", () => {
    navbar.style.display = "none";
    menuBtn.style.display = "block";
  });
  
  setTimeout(() => {
    const messagesDiv = document.querySelector('.messages');
    if (messagesDiv) {
        messagesDiv.classList.add('fade-out');
        setTimeout(() => {
            messagesDiv.style.display = 'none';
        }, 500); // Ensure the element is hidden after the fade-out transition
    }
}, 3000);
});
