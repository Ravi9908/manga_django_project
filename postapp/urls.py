from django.urls import path
from postapp import views



urlpatterns = [
    path('post_view/',views.post_view, name='posts'),
    path('edit_post/<int:post_id>/', views.edit_post, name='edit'),
    path('manga/<int:post_id>/', views.manga_detail, name='post_detail'),
    # path('manga/<int:post_id>/add_rating/', views.add_rating, name='add_rating'),
]