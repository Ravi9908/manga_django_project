from django.shortcuts import render, redirect, get_object_or_404
from postapp.forms import PostForm, RatingForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from postapp.models import Post, Rating
from django.db.models import Avg, Count


# Create your views here.


@login_required(login_url='/auth/login/')
def post_view(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()
            messages.success(request, 'Post created successfully')
            return redirect('posts')

        else:
            print(form.errors)
            messages.error(request, 'Error creating post')
    else:
        form = PostForm()
    return render(request, 'postapp/post.html', {'form': form})


@login_required(login_url='/auth/login/')
def edit_post(request, post_id):
    post = get_object_or_404(Post, id=post_id, user=request.user)
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES, instance=post)
        if form.is_valid():
            form.save()
            return redirect('profile')
    else:
        form = PostForm(instance=post)
    return render(request, 'postapp/edit_post.html', {'form': form})



def manga_detail(request, post_id):
    post = get_object_or_404(Post, id=post_id)
    url = request.META.get("HTTP_REFERER")
    if request.method == 'POST':
        try:
            reviews = Rating.objects.get(
                user__id=request.user.id, post__id=post_id)
            form = RatingForm(request.POST, instance=reviews)
            form.save()
            messages.success(request, 'your review has been updated')
            return redirect(url)
        except Rating.DoesNotExist:
            form = RatingForm(request.POST)
            if form.is_valid():
                rating = form.save(commit=False)
                rating.post = post
                rating.user = request.user
                rating.save()
                messages.success(request, 'your review has been submitted')
                return redirect(url)
            else:
                messages.error(request, 'Error submitting review')
    rating_count = Rating.objects.filter(post=post).count()
    rating_data = {i: 0 for i in range(1, 6)}
    rating_distribution = Rating.objects.filter(post=post).values(
        'rating').annotate(count=Count('rating')).order_by('-rating')
    for item in rating_distribution:
        rating_data[item['rating']] = item['count']
    print(rating_data)
    avg_rating = Rating.objects.filter(post=post).aggregate(Avg('rating'))[
        'rating__avg'] or 0
    return render(request, 'postapp/post_detail.html', {
        'post': post,
        'avg_rating': avg_rating,
        'rating_data': rating_data,
        'rating_count': rating_count
    })

