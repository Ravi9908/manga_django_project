from django.db import models
from django.contrib.auth.models import User
import os

# Create your models here.


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')
    title = models.CharField(max_length=255)
    photo = models.ImageField(upload_to='images/')
    genre = models.CharField(max_length=100)
    author = models.CharField(max_length=100, null=True)
    description = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def delete(self, *args, **kwargs):
        if self.photo:
            if os.path.isfile(self.photo.path):
                os.remove(self.photo.path)
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.title
    
class Rating(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='ratings')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.PositiveIntegerField(default=0)
    review = models.TextField(null=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.rating} by {self.user}'
